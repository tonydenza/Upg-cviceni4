﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni4._3
{
    class Program
    {
        static double[] ZaokrouhliPole(double[] pole)
        {
            for (int i = 0; i < pole.Length; i++)
                pole[i] = Math.Round(pole[i]);
            return pole;
        }

        static void Vypis(double[] pole)
        {
            for (int i = 0; i < pole.Length; i++)
            {
                if (i == pole.Length - 1)
                {
                    Console.Write(pole[i]);
                    break;
                }
                else
                    Console.Write("{0}, ", pole[i]);
            }
            Console.ReadLine();
        }

        static void Main(string[] args)
        {
            double[] pole = { 5.12, -3.5996, 156, 7.9 };
            double[] zaokrouhli = ZaokrouhliPole(pole);
            Vypis(zaokrouhli);
        }
    }
}
