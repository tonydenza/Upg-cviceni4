﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni4._5
{
    class Program
    {
        static void MaxPole(int[] pole, ref int max, ref int max2, ref int max3)
        {
            switch (pole.Length)
            {
                case 0:
                    max = max2 = max3 = -1;
                    break;
                case 1:
                    max = pole[0];
                    break;
                case 2:
                    if (pole[0] > pole[1])
                    {
                        max = pole[0];
                        max2 = pole[1];
                    }
                    else
                    {
                        max = pole[1];
                        max2 = pole[0];
                    }
                    break;
                default:
                    for (int i = 0; i < pole.Length; i++)
                    {
                        if (pole[i] > max)
                        {
                            max3 = max2;
                            max2 = max;
                            max = pole[i];
                            continue;
                        }

                        if (pole[i] > max2 && pole[i] < max || pole[i] > max2 && pole[i] == max)
                        {
                            max3 = max2;
                            max2 = pole[i];
                            continue;
                        }

                        if (pole[i] > max3 && pole[i] < max && pole[i] < max2 || pole[i] > max3 && pole[i] <= max && pole[i] <= max2)
                            max3 = pole[i];
                    }

                    break;
            }
        }

        static void Vypis(int[] pole, int max, int max2, int max3)
        {
            for (int i = 0; i < pole.Length; i++)
            {
                if (i == pole.Length - 1)
                {
                    Console.WriteLine(pole[i]);
                    break;
                }
                else
                    Console.Write("{0}, ", pole[i]);
            }

            Console.WriteLine("max = {0}, Max2 = {1}, max3 = {2}", max, max2, max3);
            Console.Read();
        }

        static void Main(string[] args)
        {
            int[] pole = { 2, 5, 7, 1, 4, 6, 9, 8, 2 };
            int max = -1, max2 = -1, max3 = -1;
            MaxPole(pole, ref max, ref max2, ref max3);
            Vypis(pole, max, max2, max3);
        }
    }
}
