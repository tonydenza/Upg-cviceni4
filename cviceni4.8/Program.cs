﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni4._8
{
    class Program
    {
        static int ZaplatitMincemi(int[] mince, int hodnota)
        {
            int pocet = 0, i = 0;
            int[] vyskyt = new int[mince.Length];

            if (mince.Length == 0)
            {
                Console.WriteLine("Nemam zadne mince.");
                return 0;
            }

            if (hodnota == 0)
                return 0;

            while (hodnota != 0)
            {
                if (hodnota - mince[i] >= 0)
                {
                    hodnota -= mince[i];
                    vyskyt[i]++;
                    pocet++;
                }
                else
                    i++;
            }

            for (int j = 0; j < vyskyt.Length; j++)
            {
                if (vyskyt[j] == 0)
                    continue;
                else
                {
                    Console.Write("{0} x {1} + ", vyskyt[j], mince[j]);
                }
            }
            Console.WriteLine("0");
            return pocet;
        }

        static void Main(string[] args)
        {
            int[] mince = { 50, 20, 10, 5, 2, 1 };
            int pocet = 0, castka1 = 97, castka2 = 88, castka3 = 26;

            pocet = ZaplatitMincemi(mince, castka1);
            Console.WriteLine("Castka {0} se da zaplatit minimalne {1} mincemi.", castka1, pocet);

            pocet = ZaplatitMincemi(mince, castka2);
            Console.WriteLine("Castka {0} se da zaplatit minimalne {1} mincemi.", castka2, pocet);

            pocet = ZaplatitMincemi(mince, castka3);
            Console.WriteLine("Castka {0} se da zaplatit minimalne {1} mincemi.", castka3, pocet);
            Console.WriteLine();

            int[] mince2 = { 50, 20, 12, 10, 5, 2, 1 };
            int castkaNeoptimalni = 36;

            pocet = ZaplatitMincemi(mince2, castkaNeoptimalni);
            Console.WriteLine("Castka {0} se da zaplatit {1} mincemi.", castkaNeoptimalni, pocet);
            Console.WriteLine("Pocet minci je vsak neoptimalni, jelikoz se dana castka {0} da zaplatit pouze 3 mincemi o hodnote 12.", castkaNeoptimalni);

            Console.ReadLine();
        }
    }
}
