﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni4._2
{
    class Program
    {
        static void AbsolutniHodnotaPole(int[] pole)
        {
            for (int i = 0; i < pole.Length; i++)
            {
                if (pole[i] < 0)
                    pole[i] *= -1;
            }
        }

        static void Vypis(int[] pole)
        {
            for (int i = 0; i < pole.Length; i++) {
                if (i == pole.Length-1)
                {
                    Console.Write(pole[i]);
                    break;
                }
                else
                    Console.Write("{0}, ", pole[i]);    
            }
            Console.ReadLine();
        }

        static void Main(string[] args)
        {
            int[] pole = { 5, -2, 0, -1, 7, -12, 2, 4};
            AbsolutniHodnotaPole(pole);
            Vypis(pole);
        }
    }
}
