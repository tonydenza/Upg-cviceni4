﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni4._9
{
    class Program
    {
        static int Sportka (int[] sazenka, int slosovani) // ladici vypisy...
        {
            int[] tah = new int[8];
            Random r = new Random();
            bool dodatkove = false;
            int trefa = 0;

            for (int i = 0; i < sazenka.Length; i++)
            {
                if (sazenka[i] < 1 || sazenka[i] > 49 || sazenka.Length > 6)
                {
                    Console.WriteLine("Spatne vyplnena sazenka, vklad se vraci.");
                    return slosovani * 20;
                }
            }

            int vyhra = -20 * slosovani;

            for (int j = 0; j < 2 * slosovani; j++)
            {
                for (int k = 0; k < tah.Length; k++)
                    tah[k] = r.Next(1, 50);

                for (int l = 0; l < sazenka.Length; l++)
                {
                    for (int m = 0; m < tah.Length; m++)
                    {
                        if (tah[6] == sazenka[l])
                            dodatkove = true;

                        if (tah[m] == sazenka[l])
                            trefa++;
                    }
                }

                switch (trefa)
                {
                    case 6:
                        vyhra += 15382198;
                        break;
                    case 5:
                        if (dodatkove)
                            vyhra += 815723;
                        else
                            vyhra += 24971;
                        break;
                    case 4:
                        vyhra += 619;
                        break;
                    case 3:
                        vyhra += 113;
                        break;
                }

                trefa = 0;
                dodatkove = false;
            }

            return vyhra;
        }

        static void Main(string[] args)
        {
            int[] sazenka = { 7, 9, 22, 6, 14, 48 };
            int vys = Sportka(sazenka, 5000);

            Console.WriteLine("Vase bilance: {0}", vys);
            Console.Read();
        }
    }
}
