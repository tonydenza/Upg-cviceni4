﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni4._7
{
    class Program
    {
        static int SkalarniSoucin( int[] u, int[] v)
        {
            int skalar = 0;

            if (u.Length == 0 || v.Length == 0)
            {
                Console.WriteLine("Jeden ci oba vektory nemaji zadne prvky.");
                return 0;
            }

            if (u.Length != v.Length)
            {
                Console.WriteLine("Vektory nemaji stejny pocet prvku.");
                return 0;
            }

            for (int i = 0; i < u.Length; i++)
                skalar += u[i] * v[i]; 

            return skalar;
        }

        static void Vypis(int[] pole)
        {
            for (int i = 0; i < pole.Length; i++)
            {
                if (i == pole.Length - 1)
                {
                    Console.WriteLine(pole[i]);
                    break;
                }
                else
                    Console.Write("{0}, ", pole[i]);
            }
        }

        static void Main(string[] args)
        {
            int[] u = { 2, 0, -5, 1, 4 };
            int[] v = { 7, 4, 2, -2, 0 };
            int skalar = SkalarniSoucin(u, v);
            Vypis(u);
            Vypis(v);
            Console.Write("Skalarni soucin: {0}", skalar);
            Console.ReadLine();
        }
    }
}
