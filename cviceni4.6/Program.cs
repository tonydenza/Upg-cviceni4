﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni4._6
{
    class Program
    {
        static int NejcastejsiPrvek(int[] pole)
        {
            if (pole.Length == 0)
                return 0;

            int[] pole2 = new int[pole.Length];
            int max = int.MinValue, k = 0, kacko = 0;

            for (int i = 0; i < pole.Length; i++)
            {
                for (int j = 0; j < pole.Length; j++)
                {
                    if (pole[i] == pole[j])
                        pole2[i]++;
                }
            }

            for (k = 0; k < pole2.Length; k++)
            {
                if (pole2[k] > max)
                {
                    max = pole2[k];
                    kacko = k;
                }

            }

            return pole[kacko];
        }

        static void Vypis(int[] pole, int modus)
        {
            for (int i = 0; i < pole.Length; i++)
            {
                if (i == pole.Length - 1)
                {
                    Console.WriteLine(pole[i]);
                    break;
                }
                else
                    Console.Write("{0}, ", pole[i]);
            }

            Console.WriteLine("modus = {0}", modus);
            Console.Read();
        }

        static void Main(string[] args)
        {
            int[] pole = { 2, 7, 6, 6, 6, 1, 7, 7, 2};
            int modus;
            modus = NejcastejsiPrvek(pole);
            Vypis(pole, modus);
        }
    }
}
