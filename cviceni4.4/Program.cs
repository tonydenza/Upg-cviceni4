﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni4._4
{
    class Program
    {
        static void ObratPole(int[] pole) // bez pomocneho pole!
        {
            int[] pole2 = new int[pole.Length];
            int i = 0;

            for (int j = 0; j < pole.Length; j++)
            {
                pole2[j] = pole[i];
                i++;
            }

            for (i = 0; i < pole.Length; i++)
                pole[i] = pole2[pole2.Length - i - 1];
        }

        static void Vypis(int[] pole)
        {
            for (int i = 0; i < pole.Length; i++)
            {
                if (i == pole.Length - 1)
                {
                    Console.Write(pole[i]);
                    break;
                }
                else
                    Console.Write("{0}, ", pole[i]);
            }
            Console.ReadLine();
        }

        static void Main(string[] args)
        {
            int[] pole = { 5, 7, 2, 1, 0, 6 };
            ObratPole(pole);
            Vypis(pole);
        }
    }
}
