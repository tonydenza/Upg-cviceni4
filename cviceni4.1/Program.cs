﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni4._1
{
    class Program
    {
        static void SetridSestupne(ref int a, ref int b)
        {
            if (b > a)
            {
                int tmp = a;
                a = b;
                b = tmp;
            }
        }

        static void Main(string[] args)
        {
            int a = 5, b = 7;
            SetridSestupne(ref a, ref b);
            Console.WriteLine("{0}, {1}", a, b);
            Console.ReadLine();
        }
    }
}
